package models

type Model struct {
	Model interface{}
}

func RegisterModels() []Model {
	return []Model{
		{Model: User{}},
		{Model: Address{}},
		{Model: Order{}},
		{Model: OrderItem{}},
		{Model: Payment{}},
		{Model: Product{}},
		{Model: Category{}},
		{Model: Section{}},
		{Model: ProductImage{}},
		{Model: Cart{}},
		{Model: CartItem{}},
	}
}
