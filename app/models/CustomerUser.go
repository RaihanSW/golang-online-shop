package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID         string `gorm: "size:36;not null;uniqueIndex;primary_key "`
	Addresses  []Address
	Orders     []Order
	FirstName  string `gorm: "size:100;not null"`
	LastName   string `gorm: "size:100;not null"`
	Email      string `gorm: "size:100;not null;uniqueIndex"`
	Password   string `gorm: "size:255;not null"`
	Phone      string `gorm: "size:20"`
	Created_at time.Time
	Updated_at time.Time
	Deleted_at gorm.DeletedAt
}
