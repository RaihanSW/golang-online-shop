package models

import (
	"time"

	"github.com/shopspring/decimal"
	"gorm.io/gorm"
)

type Order struct {
	ID             string          `gorm: "size:36;not null;uniqueIndex;primary_key "`
	Status         string          `gorm: "size:100;not null"`
	Payment_status string          `gorm: "size:100;not null"`
	Note           string          `gorm: "type:text"`
	Total_payment  decimal.Decimal `gorm: "type:decimal(20,2)"`
	Approved_by    string          `gorm: "size:100"`
	Payment_due    time.Time
	Order_date     time.Time
	Approved_at    time.Time
	Created_at     time.Time
	Updated_at     time.Time
	Deleted_at     gorm.DeletedAt
	User           User
	UserID         string `gorm: "size:100";index`
	OrderItems     []OrderItem
}
