package models

import (
	"time"

	"gorm.io/gorm"
)

type Section struct {
	ID         string `gorm: "size:36;not null;uniqueIndex;primary_key "`
	Name       string `gorm: "size:100;not null"`
	Created_at time.Time
	Updated_at time.Time
	Deleted_at gorm.DeletedAt
	Categories []Category
}
