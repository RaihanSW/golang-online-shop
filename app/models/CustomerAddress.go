package models

import (
	"time"

	"gorm.io/gorm"
)

type Address struct {
	ID         string `gorm: "size:36;not null;uniqueIndex;primary_key "`
	User       User
	UserID     string `gorm: "size:100";index`
	City       string `gorm: "size:100;not null"`
	Province   string `gorm: "size:100"`
	Postcode   string `gorm: "size:100"`
	Created_at time.Time
	Updated_at time.Time
	Deleted_at gorm.DeletedAt
}
