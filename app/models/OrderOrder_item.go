package models

import (
	"time"

	"github.com/shopspring/decimal"
	"gorm.io/gorm"
)

type OrderItem struct {
	ID             string          `gorm: "size:36;not null;uniqueIndex;primary_key "`
	Name           string          `gorm: "size:100"`
	Quantity       decimal.Decimal `gorm: "type:decimal(20,2)"`
	Base_price     decimal.Decimal `gorm: "type:decimal(16,2)"`
	Subtotal_price decimal.Decimal `gorm: "type:decimal(20,2)"`
	Created_at     time.Time
	Updated_at     time.Time
	Deleted_at     gorm.DeletedAt
	Order          Order
	Product        Product
	OrderID        string `gorm: "size:100";index`
	ProductID      string `gorm: "size:100";index`
}
