package models

import (
	"time"

	"gorm.io/gorm"
)

type Category struct {
	ID         string `gorm: "size:36;not null;uniqueIndex;primary_key "`
	Name       string `gorm: "size:100;not null"`
	Created_at time.Time
	Updated_at time.Time
	Deleted_at gorm.DeletedAt
	Products   []Product `gorm:"many2many:product_categories;"`
	Section    Section
	SectionID  string `gorm: "size:100";index`
}
