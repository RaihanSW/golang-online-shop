package models

import (
	"time"

	"github.com/shopspring/decimal"
	"gorm.io/gorm"
)

type Product struct {
	ID               string          `gorm: "size:36;not null;uniqueIndex;primary_key "`
	Name             string          `gorm: "size:100;not null"`
	Price            decimal.Decimal `gorm: "type:decimal(16,2)"`
	Description      string          `gorm: "type:text"`
	ShortDescription string          `gorm: "size:255"`
	Weight           decimal.Decimal `gorm: "type:decimal(10,2)"`
	Status           int             `gorm: "default:0"`
	Type             string          `gorm: "size:64"`
	Stock            int             `gorm: "default:0"`
	ImagePath        string          `gorm: "type:text"`
	Created_at       time.Time
	Updated_at       time.Time
	Deleted_at       gorm.DeletedAt
	Category         []Category `gorm:"many2many:product_categories;"`
	OrderItems       []OrderItem
	ProductImages    []ProductImage
}

func (p *Product) GetProducts(db *gorm.DB, perPage int, page int) (*[]Product, int64, error) {
	var err error
	var products []Product
	var count int64

	err = db.Debug().Model(&Product{}).Count(&count).Error
	if err != nil {
		return nil, 0, err
	}

	offset := (page - 1) * perPage

	err = db.Debug().Model(&Product{}).Order("created_at desc").Limit(perPage).Offset(offset).Find(&products).Error
	if err != nil {
		return nil, 0, err
	}

	return &products, count, nil
}

func (p *Product) FindByID(db *gorm.DB, productID string) (*Product, error) {
	var err error
	var product Product

	err = db.Debug().Preload("ProductImages").Model(&Product{}).Where("id = ?", productID).First(&product).Error
	if err != nil {
		return nil, err
	}

	return &product, nil
}
