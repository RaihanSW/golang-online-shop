package models

import (
	"time"

	"github.com/shopspring/decimal"
	"gorm.io/gorm"
)

type Payment struct {
	ID            string          `gorm: "size:36;not null;uniqueIndex;primary_key "`
	Method        string          `gorm: "size:100;not null"`
	Total_payment decimal.Decimal `gorm: "type:decimal(20,2)"`
	Number        string          `gorm:"size:100;index"`
	Status        string          `gorm:"size:100"`
	Created_at    time.Time
	Updated_at    time.Time
	Deleted_at    gorm.DeletedAt
	Order         Order
	OrderID       string `gorm: "size:100";index`
}
