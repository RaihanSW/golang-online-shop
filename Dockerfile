FROM golang:1.19.5
RUN mkdir /app
ADD . /app
WORKDIR /app

RUN go mod download
RUN go run main.go db_migrate
RUN go run main.go db_seeding
RUN go build -o main .

EXPOSE 9000

CMD ["/app/main"]