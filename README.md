# Golang Online Shop

# Description
A Golang project to create an online shop. Features made are:
- User
contain the data of user along with their address

- Product list
save product along with their picture as showcase

- Cart
Ordered products are saved in a cart. User can check the total payment needed


# How to run
1. Pull the codebase, or download it as zip from git
2. Set the .env file for environment, you can get the file from deleting .example from ".env.example"
3. Run "go run main.go db_migrate" to migrate all of database needed in the project
4. Run "go run main.go db_seeding", a seed is created using "faker" to insert dummy data into database
5. Run "go run main.go" to enable the server
6. At default, the server run on localhost:9000 (can be changed depend on user environment)

# Another Source

The ERD for the project can be found here:
https://drive.google.com/file/d/1f_eYncDyJwK3swLb8SlIutRwyTKqQlVU/view?usp=sharing

The best practice for API documentation is using swagger. But since I'm new to golang, Postman collection was used for the API documentation which can be found here:
https://drive.google.com/file/d/1Jzq7lbN7nMXvT_gP_L3vjH8SmquEV92n/view?usp=sharing

to use the Postman collection, One has to download "Postman" for API testing, and has the project run on the server localhost

# Reference
Source for the Front-End .tmpl layout :
https://github.com/gieart87/gotoko/tree/feature/shopping-cart/templates