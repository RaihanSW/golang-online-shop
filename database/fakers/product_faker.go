package fakers

import (
	"math"
	"math/rand"
	"time"

	"github.com/bxcodec/faker/v3"
	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"gitlab.com/RaihanSW/Online_store/app/models"
	"gorm.io/gorm"
)

func ProductFaker(db *gorm.DB) *models.Product {
	name := faker.Name()
	return &models.Product{
		ID:               uuid.New().String(),
		Name:             "Book title:" + name,
		Price:            decimal.NewFromFloat(randFloats(1.10, 101.98, 5)[0]),
		Stock:            rand.Intn(100),
		Weight:           decimal.NewFromFloat(rand.Float64()),
		ShortDescription: faker.Paragraph(),
		Description:      faker.Paragraph(),
		Status:           1,
		Created_at:       time.Time{},
		Updated_at:       time.Time{},
		Deleted_at:       gorm.DeletedAt{},
	}
}

func fakePrice() float64 {
	return precision(rand.Float64()*math.Pow10(rand.Intn(8)), rand.Intn(2)+1)
}

// precision | a helper function to set precision of price
func precision(val float64, pre int) float64 {
	div := math.Pow10(pre)
	return float64(int64(val*div)) / div
}

func randFloats(min, max float64, n int) []float64 {
	res := make([]float64, n)
	for i := range res {
		res[i] = min + rand.Float64()*(max-min)
	}
	return res
}
