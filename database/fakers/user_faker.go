package fakers

import (
	"time"

	"github.com/bxcodec/faker/v3"
	"github.com/google/uuid"
	"gitlab.com/RaihanSW/Online_store/app/models"
	"gorm.io/gorm"
)

func UserFaker(db *gorm.DB) *models.User {
	return &models.User{
		ID:         uuid.New().String(),
		FirstName:  faker.FirstName(),
		LastName:   faker.LastName(),
		Email:      faker.Email(),
		Password:   "password", // password
		Created_at: time.Time{},
		Updated_at: time.Time{},
		Deleted_at: gorm.DeletedAt{},
	}
}
